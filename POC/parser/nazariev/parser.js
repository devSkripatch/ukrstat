var extractor = require('extractor'),
	cheerio = require('cheerio'),
	FS = require('fs'),		
	PATH = require('path'),
	pages = [];

	String.prototype.foldWhiteSpaces = function(){
		return this.replace(/\s+/g, ' ').replace(/^\s|\s$/g, '');
	}

	function convertSup(sup){
		var txt,
			num;
		txt = sup.text();
		if (txt.length === 1) {
			num = parseInt(txt, 10);
			num   === 1 ? sup.text('¹')
			: num === 2 ? sup.text('²')
			: num === 3 ? sup.text('³')
			: num === 4 ? sup.text('⁴')
			: num === 5 ? sup.text('⁵')
			: num === 6 ? sup.text('⁶')
			: num === 7 ? sup.text('⁷')
			: num === 8 ? sup.text('⁸')
			: num === 9 ? sup.text('⁹')
			: sup.text('⁰');
		} else if (txt.length > 1) {
			txt = sup.text();
			txt = txt.replace('1', '¹');
			txt = txt.replace('2', '²');
			txt = txt.replace('3', '³');
			txt = txt.replace('4', '⁴');
			txt = txt.replace('5', '⁵');
			txt = txt.replace('6', '⁶');
			txt = txt.replace('7', '⁷');
			txt = txt.replace('8', '⁸');
			txt = txt.replace('9', '⁹');
			sup.text(txt);
		}
	}

	function extractFieldsFrom($table){
		var values = {},
			graphs = [],
			$tr = $table.find('tr'),
			$td = $table.find('td'),
			titleByNumber = {
				length: 0
			},
			keyByNumber = {},
			data,
			len,
			f = $tr.first().find('td').slice(1, -1);

		f.each(function(index, element){
			titleByNumber[index] = this.text().foldWhiteSpaces();
			titleByNumber.length = titleByNumber.length + 1;
		});

		len = titleByNumber.length;
		data = new Array(len);
		while (len--) {
			data[len] = [];
		}

		$tr.slice(1, -1).each(function(trIndex, trElement){
			var tdList = this.find('td'),
				key;

			key = tdList.first().text().foldWhiteSpaces() + '/' + tdList.last().text().foldWhiteSpaces();

			tdList.slice(1, -1).each(function(index, tdElement){
				var sup = this.find('sup');
				if (sup.length) {
					convertSup(sup);
				}
				data[index].push({
					key: key,
					value: this.text().foldWhiteSpaces()
				});
			});

		});

		len = titleByNumber.length;
		while (len--){
			graphs.push({
				title: titleByNumber[len],
				data: data[len]
			});
		}

		return graphs;
	}

	function extractNotes($table){
		var notes = [];

		$table.find('td').first().find('p').each(function(){
			var sup = this.find('sup').first();
			if (sup.length) {
				convertSup(sup);
			}
			notes.push(this.text().foldWhiteSpaces());
		});

		return notes;
	}

extractor.registerParser({
	parser: function (settings, onComplete) {
		var body = cheerio.load(settings.file),
			fileName = PATH.basename(settings.path),
			$tablesList = body('table'),
			page = {
				title: body('.Section1 > p').slice(0, 3).text().foldWhiteSpaces(),
				url: settings.path,
				graphs: [],
				dimension: body('.Section1 > p').eq(6).text().foldWhiteSpaces(),
				notes: []
			};

		$tablesList.each(function(index, element){
			if (this.find('tr').length > 1){
				page.graphs.push( extractFieldsFrom(this) );
			} else {
				page.notes = page.notes.concat( extractNotes(this) );
			}
		});

		pages.push(page);
		console.log(JSON.stringify(page));
		FS.writeFile('./values.json', JSON.stringify(page));/*pages*/
		
		onComplete(page);
	},
	paths: ['**/iosep/*0*.html'],
	name: 'Nazariev extractor'
})
//FS.writeFile('./values.json', JSON.stringify(pages));