var Page = {
	// Заголовок страницы
	title : String,

	// Путь к файлу
	url : String,

	// Массив графиков страницы
	graphs : Array(Graph),

	// Размерность графиков
	dimension : 'килькисть осиб',

	// Заметки (сноски и вся хуйня)
	notes : Array(String)
}


// Объект графика
var Graph = {
	// Верхушка (заголовок колонки)
	title : String,

	// Данные колонки
	data : Array({
		key : String,
		value : Number
	})
}

// Пример ответа с первой колонки файла
// file:///Users/dmitry/Dropbox/projects/ukrstat/POC/parser/podgorniy/parser-example/data/ds/kn/kn_u/122003.html
data : [{
	title : 'Все населення',
	data : [{
		key : 'Украина',
		value : 23573
	}, {
		key : 'АРК',
		value : 452
	}, {
		key : 'Винница',
		value : 574
	}]
}, {
	title : 'миське',
	data : [{
		key : 'Украина',
		value : 6736
	}]
}]