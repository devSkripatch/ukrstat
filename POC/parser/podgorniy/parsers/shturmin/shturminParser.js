// i turn off strict mode because of function declaration in object metod. it`s not right, but now it is temporary workflow.
//'use strict'

var extractor = require('extractor');

var log = function() {
	return console.log.bind(console);
}

extractor.registerParser({
	parser: function(settings, onComplete) {
		var validationOk;
		var fileContents;
		var filePath;
		var result;
		var html_data;
		var cheerio = require('cheerio');
		// html string
		fileContents = settings.file;
		
		// full path to file 
		filePath = settings.path;

		//empty result
		result = {
			title: '',
			table_title: '',
			url: filePath,
			graphs: [{
				title: '',
				data: []
			}],
			// Размерность графиков
			'dimension': '',
			// Заметки (сноски и вся хуйня)
			'notes': ''
		};


function Page_data_gatherer(fileContents) {
	//console.log(url);
	this.$ = cheerio.load(fileContents, {
			ignoreWhitespace: true,
			lowerCaseTags: true
		});
	this.doc_url = "";
	this.title_text = {};
	this.nodes = [];
	this.number_of_tables = 0;
	this.tables_array = [];
	this.pages_with_tables = [];
	this.pages_without_tables = [];
	this.table_data = {};
	this.number_names = [];
	// for graph test object
	this.graph_obj = {
		Page: {
			// Заголовок страницы
			'title': 'String',
			'table_title': "",
			// Путь к файлу
			'url': 'String',

			// Массив графиков страницы
			'graphs': [],

			// Размерность графиков
			'dimension': 'килькисть осиб',

			// Заметки (сноски и вся хуйня)
			'notes': 'Array(String)'
		},
		//page_title: "",
		//page_url: "",
		chart_name: "",
		/*x_coordinates_description: this.number_names,
		y_coordinates_quantity_unit: "",
		y_coordinates_description_text: "",*/
		// Page.graphs : [{
		// 			name: 'Склад валового внутрішнього продукту',
		// 			data: [17.0, null, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 91.6],
		// 			visible: false
		// }]
	}
	// end test graph object
	this.init();
}

Page_data_gatherer.prototype.init = function() {
		this.collect_data_from_file();
};

Page_data_gatherer.prototype.is_table = function(node) {
	//console.log("is table?");
	if (this.$(node).find('tr').length > 3) {
		return true;
	} else {
		return false;
	}
};

Page_data_gatherer.prototype.find_tables = function() {
	//console.log("find");
	var tables_nodes = this.$('table');
	for (var i = 0; i < tables_nodes.length; i++) {
		if (this.is_table(tables_nodes[i])) {
			this.tables_array.push(this.$(tables_nodes[i]));
			this.number_of_tables += 1;
			//console.log(this.number_of_tables);
		}
	}
};

Page_data_gatherer.prototype.is_page_needed = function(page) {
	//console.log("is page need?");
	this.find_tables();

	if (this.number_of_tables < 1) {
		this.pages_without_tables.push(this.doc_url);
		return false;
		//  console.log("no data");
	} else {
		this.pages_with_tables.push(this.doc_url);
		return true;
	}
};

Page_data_gatherer.prototype.exclude_archive_node = function() {
	// console.log("del archive");
	var arc_tbl = this.$('table');
	if (arc_tbl.length >= 2) {
		if (!this.is_table(arc_tbl[0])) {
			this.$(arc_tbl[0]).remove();
			//console.log('del' + this.$(arc_tbl[0]).html());
		}
	} else {
		return;
	}
};

Page_data_gatherer.prototype.gather_titles = function() {
	//console.log("gather titles");
	//var doc_title = this.$('title').text();
	this.graph_obj.Page['title'] = this.$('title').text();

	var how_much_tables = this.number_of_tables;
	var i = 0;
	// console.log("tables-> " + how_much_tables, "nodes length- "+this.doc_nodes.length);

	for (var j = 0; j < how_much_tables; j++) {
		// console.log('j = ' +j );
		//для каждой таблицы соберем данные перед таблицей
		for (; i < this.doc_nodes.length; ++i) {
			// console.log('i = ' +i , this.doc_nodes.length);

			if (this.$(this.doc_nodes[i]).find('tr').length > 3) {
				// console.log('this_node_is_table - delete');
				//this.$(this.doc_nodes[i]).remove();

				i = i + 1;
				break;
			} else {
				// console.log('try check');
				if (this.$(this.doc_nodes[i]).text().replace(/\s+|\n+/g, " ").length > 10) {
					//   console.log('first step');
					if (!/\*\)/.exec(this.$(this.doc_nodes[i]).text())) {
						// console.log('parsing' + this.$(this.doc_nodes[i]).html());
						var parsed = this.$(this.doc_nodes[i]).text().replace(/\s+|\n+/g, " ");
						var cleared = parsed.replace(/:*\s{2}\|+/g, "");
						if (this.title_text['table ' + j]) {
							this.title_text['table ' + j] += " " + cleared;
						} else {
							this.title_text['table ' + j] = cleared;
						}
						//console.log('j = ' + j, "i = "+i);
						this.nodes.push(cleared);
					}
				}
			}
		}
	}
};

Page_data_gatherer.prototype.parse_table = function() {

	var self = this;
	var tables_to_parse = this.tables_array;
	var table,
		table_rows;

	for (var i = 0; i < tables_to_parse.length; i++) {
		table = tables_to_parse[i];
		self.extractDataFromTableHead(table);
		table_rows = this.$(table).find('tr');

		function parse_table_row(row_cells) {
			var row_name = self.$(row_cells[0]);

			if (self.$(row_cells[0]).text().indexOf('у тому') + 1 || self.$(row_cells[0]).text().indexOf('з неї') + 1) {
				return;
			} else {
				for (var k = 1; k < 4; k++) {
					//how_many_rows_in_table[k];
					self.graph_obj.Page.graphs.push({
						name: row_name.text(),
						data: [{
							name: self.namesArr[k - 1],
							data: +self.$(row_cells[k]).text().replace(",", ".")
						}],
					});
				};

			}
		}

		function extract_nubmer_names(row_cells) {
			self.number_names = [];
			for (var k = 1; k < 4; k++) {
				self.number_names.push(self.$(row_cells[k]).text().replace("\r", "").replace("\n", ""));
			};
		};

		for (var j = 1; j < table_rows.length; j++) {
			var row_cells = this.$(table_rows[j]).find('td');

			if (j === 1) {
				extract_nubmer_names(row_cells);
				continue;
			}
			parse_table_row(row_cells);
		};
	};
}

Page_data_gatherer.prototype.collect_data_from_file = function() {
	var self = this;
		self.doc_nodes = self.$('body>*');
		if (self.$('.Section1').length) {
			self.doc_nodes = self.$('.Section1>*');
			console.log('s');
		}
		if (self.$('html>*').length > 4) {
			self.doc_nodes = self.$('head').nextAll();

			console.log('h');
		}

		if (self.is_page_needed()) {
			self.exclude_archive_node();
			self.gather_titles();
			self.parse_table();
		}
		var tables_html_string = self.tables_array.toString();
		var dt = JSON.stringify(self.table_data);
		self.graph_obj.Page.table_title = self.title_text;
		var tt = JSON.stringify(self.title_text);
		self.graph_obj.chart_name = self.title_text['table 0'];
		var moc_string = JSON.stringify(self.graph_obj);

		result = self.graph_obj.Page;
};

Page_data_gatherer.prototype.extractDataFromTableHead = function(table) {
	this.namesArr = [];
	var self = this;
	var firstRow = this.$(this.$(table).find('tr')[0]),
		firstCell = this.$(firstRow.find('td')[0])	,
		rowSpan = firstCell.attr('rowspan'),
		rows = rowSpan && rowSpan > 1 ? rowSpan : 0;
	var names = {

	};

	if (rows) {
		var tableRows = this.$(table).find('tr');

		for (var i = 0; i < rows; i++) {
			var rowCells = this.$(tableRows[i]).find('td');
			names[i] = names[i] || [];

			this.$(rowCells).each(function(index, elem) {
				var colspanAttr = self.$(elem).attr('colspan');
				var rowspan = self.$(elem).attr('rowspan');
				var iterationNum = colspanAttr && colspanAttr > 1 ? colspanAttr : 1;
				var indexToInsert;

				for (var j = 0; j < iterationNum; j++) {
					var is_finded = false;
					for (var k = 0; k < names[i].length; k++) {
						if (!names[i][k]) {
							names[i][k] = self.$(elem).text();
							is_finded = true;
							break;
						}
					}
					if (!is_finded) {
						names[i].push(self.$(elem).text());
					}
					indexToInsert = names[i].length - 1;
				}
				// если у ячейки есть rowspan , мы в следующий массив, с таким же индексом вставляем пустое значение.
				// для синхронизации длины массивов перед сбором ополных писаний данных в ячейке. 
				if (rowspan && rowspan > 1) {
					//если массив есть, присваиваем значение элементу с заданным индексом
					for (var h = 1; h < rowspan; h++) {
						if (names[i + h]) {
							names[i + h][indexToInsert] = " ";
						} else {
							// если массива еще нет, мы создаем его и пушим значение.
							names[i + h] = [];
							names[i + h][indexToInsert] = " ";
						}
					};

				}
			})
		}

		for (var nameArr in names) {
			for (var i = 0; i < names[nameArr].length; i++) {
				//console.log(names[nameArr][i]);
				if (self.namesArr[i]) {
					self.namesArr[i] += names[nameArr][i];
				} else {
					self.namesArr[i] = "";
					self.namesArr[i] += names[nameArr][i];
				}

			};
		}
		console.log(self.namesArr);
	}
}

my_Page_data_gatherer = new Page_data_gatherer(fileContents);



		validationOk = onComplete(result);

		if (!validationOk) {
			console.log(result)
		}
	},
	// paths: ['/Users/dmitry/Dropbox/projects/data/operativ/operativ2011/fin/fin_rez/fr_pr/fr_pr_r/arh_fr_pro2011_r.html'],
	paths: ['**/test/*.htm*'],
	name: 'test extractor'
})