'use strict'

var extractor = require('extractor')

var log = function () {
	return console.log.bind(console);
}

extractor.registerParser({
	parser: function (settings, onComplete) {
		var validationOk;
		var fileContents;
		var filePath;

		var result;

		result = {
			title: Math.random() > 0.5 ? 'Укринские горки' : '',
			url: 'http://google.com',
			graphs: [{
					title: 'foo',
					data: []
				}, {
				title: 'boo',
				data: [{
					key: 'Украина',
					value: '2300'
				}, {
					key: 'asdasd',
					value: 'asdasdas'
				}]
			}]
		};

		fileContents = settings.file;
		filePath = settings.path;

		validationOk = onComplete(result);

		if (!validationOk) {
			console.log(result)
		}
	},
	// paths: ['/Users/dmitry/Dropbox/projects/data/operativ/operativ2011/fin/fin_rez/fr_pr/fr_pr_r/arh_fr_pro2011_r.html'],
	paths: ['**/*.js'],
	name: 'test extractor'
})
