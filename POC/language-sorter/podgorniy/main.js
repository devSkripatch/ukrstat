'use strict'

var file = require('file')
var fs = require('fs')
var path = require('path')
var cheerio = require('cheerio')
var request = require('request')
var async = require('async')

var srcFolder = './data/raw-data'
var targetFolder = './data/processed-data'

var yandexTranslateAPIKey = 'trnsl.1.1.20130512T104455Z.8a0ed400b0d249ba.48af47e72f40c8991e4185556b825273d104af68';
var yandexAPIUrl = 'https://translate.yandex.net/api/v1.5/tr.json/detect';

var languageFolders = {
	'en' : 'en',
	'ru' : 'ru',
	'uk' : 'uk'
};

function collectAllFilesReferences () {
	var res = [];

	file.walkSync(srcFolder, function (path, folder, files) {
		res = res.concat(files.map(function (fileName) {
			return path + '/' + fileName
		}))
	});
	return res;
}


var allFiles = collectAllFilesReferences();
var filesProcessors;


filesProcessors = allFiles.map(function (filePath) {
	return function (nextWorker) {
		recognizeLanguage(filePath, function (language) {
			saveToLangFolder(filePath, language, nextWorker);
		})
	}
})

async.parallelLimit(filesProcessors, 5);






function saveToLangFolder(filePath, lang, onComplete) {
	var normalPath = path.normalize(filePath);
	var targetPath;

	if (!languageFolders[lang]) {
		console.log(lang);
	}

	targetPath = normalPath.split(path.sep);
	targetPath.shift()
	targetPath = targetFolder + '/' + (languageFolders[lang] ? languageFolders[lang] : 'uncategorized') + '/' + targetPath.join(path.sep);

	createDirectories(targetPath);

	fs.readFile(filePath, 'utf-8', function (err, fileBody) {
		if (err) {
			console.log(err);
		}

		fs.writeFile(targetPath, fileBody, onComplete);
	});
} 

var createdPaths = {};


function createDirectories (filePath) {
	var dirName = path.normalize(path.dirname(filePath));

	if (dirName in createdPaths) {
		return;
	}

	dirName.split(path.sep).forEach(function (dirName, index, pathArr) {

		var folderPath;

		folderPath = pathArr.slice(0,index + 1).join(path.sep)
		if (!fs.existsSync(pathArr.slice(0,index + 1).join(path.sep))) {
			fs.mkdirSync(folderPath)
		}
	})
	console.log('Directory created: ', dirName);
	createdPaths[dirName] = true;
}


function recognizeLanguage (file, onRecognitionComplete) {
	var text;
	var translateRequestString;

	fs.readFile(file, 'utf-8', function (err, fileContent) {
		var text;

		if (err) {
			console.log(err);
		}

		text = getTextSample(fileContent);
		// console.log(text);
		translateRequestString = formDetectStringRequest(text);
		request(translateRequestString, function (err, resp) {
			var language;
			if (err) {
				console.log(err);
			}

			try {
				language = JSON.parse(resp.body).lang;
			} catch (err) {
				console.log('response error');
				console.log(resp.body);
				console.log(file);
				process.exit(1);
			}
			if (!language) {
				console.log('Language not recognized');
				console.log(text);
				console.log('-------');
			}
			onRecognitionComplete(language);
		});
	});
}


function getTextSample (htmlContents) {
	return cheerio('*', htmlContents).text().replace(/\s+/g, ' ').substr(0, 500);
}


function formDetectStringRequest (text) {
	var requestString;

	requestString = yandexAPIUrl + '?key=' + yandexTranslateAPIKey + '&text=' + encodeURIComponent(text);

	return requestString;
}