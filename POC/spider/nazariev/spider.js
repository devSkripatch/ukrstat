/* jshint node: true */
var FS = require('fs'),					//standart lib for filesystem
	PATH = require('path'),				//standart lib for filenames
	HTTP = require('http'),				//standart lib for http client-server
	URL = require('url'),				//standart lib
	MKDIRP = require('mkdirp'),			//extend lib for creating directories recursively
	ICONV = require('iconv'),			//extend lib for encoding convertor
	//html = require('htmlparser2'),	//extend lib for parsing raw html text to DOM
	//select = require('soupselect'),	//extend lib for working with DOM
	CHEERIO = require('cheerio'),

	spider = new process.EventEmitter(),			//event machine
	conv = new ICONV.Iconv('windows-1251', 'utf8'),
	collection = {
		knownLinks : [],
		pages: [],
		archiveLinks: []
	},
	stackLinks = [],
	config,
	begin = new Date(),
	currentRequestCount = 0,
	getPage = function(path) {
		var url = 'http://' + config.HOST + path,
			responseHandler = function (response) {
				response.setEncoding('binary');

				var body = '';

				response.on('data', function (chunk) {
					body += chunk;
				});

				response.on('end', function() {
					var error;
					console.log('Converting encoding...', url);
					try {
						body = new Buffer(body, 'binary');
						body = conv.convert(body).toString().replace('windows-1251', 'UTF-8');
					} catch (errorMsg) {
						console.error('Converting fail', url);
						error = 'Encoding error';
					} finally {
						spider.emit('pageLoad', url, body, error);
						currentRequestCount--;
					}
				});

			};

		HTTP.get({host: config.HOST, path: path, encoding: 'binary'}, responseHandler);
		currentRequestCount++;
	},
	saveDataToFile = function(filename, data){
		filename = config.HOST + filename;
		FS.writeFileSync(filename, data);
	},
	saveHTML = function(path, body){
		var dir = PATH.dirname(path);
		MKDIRP(config.HOST + dir, function(err){
			if (err){
				console.error(err);
			} else {
				saveDataToFile(path, body);
			}
		});
	},
	parsePage = function(baseUrl, body, error){
		function getLinks(){

			console.log('START');

			$dom('a[href]').each(function(linkNode){
				// console.log('ASDASDASD');
				var rel_link = this.attr('href'),
					abs_link,
					linkUrlObj;

				// console.log(arguments, arguments.length);
				// console.log('Reseloving...', baseUrl, rel_link);

				console.log('URLS:', baseUrl, rel_link);
				abs_link = URL.resolve(baseUrl, rel_link);
				linkUrlObj = URL.parse(abs_link);


				if (linkUrlObj.host === config.HOST &&
					linkUrlObj.path &&
					linkUrlObj.path.indexOf('.htm') !== -1 &&
					collection.knownLinks.indexOf(linkUrlObj.path) === -1){

					stackLinks.push(linkUrlObj.path);
					collection.knownLinks.push(linkUrlObj.path);
				} else if (	linkUrlObj.host === config.HOST/* &&
					linkUrlObj.path.indexOf('.zip') !== -1 ||
					linkUrlObj.path.indexOf('.rar') !== -1 */){
					collection.archiveLinks.push(linkUrlObj.href);
				}
			});
		}

		console.log('Parsing...', baseUrl);
		var $dom,
			baseUrlObj = URL.parse(baseUrl),
			$caption,
			captionText,
			object;

		// console.log(baseUrlObj);
		
		object = {
			errors : [],
			url : baseUrlObj.path
		};

		if (error){
			object.errors.push(error);
		}

		try {
			$dom = CHEERIO.load(body);
			getLinks();
			$caption = $dom('.MsoTitle span'),
			captionText = $caption.length ? $caption.map(function(){ return this.text();}).join():
				$dom('.MsoNormal b span').text(),
			(
				object.title = $dom('title').text(),
				object.caption = captionText,
				// object.tables: $dom('.MsoNormalTable'),
				object.isDataPresent = !!$dom('.MsoNormalTable').length || (baseUrl.indexOf('operativ/operativ') !== -1 && baseUrl.indexOf('arh_') === -1)
			)
		} catch (errorMsg) {
			object.errors.push(errorMsg);
			console.error('DOM building fail', baseUrl);
		} finally {
			collection.pages.push(object);
			FS.appendFileSync('./logobj.json', JSON.stringify(object) + ',\n');

			console.log("Saving:  ", baseUrlObj.path);
			saveHTML(baseUrlObj.path, body); 
		}

	},
	traversePages =function(){ 
		var curr, i=0;
		// exit if no more links and no pending requests
		// or loaded pages count reaches value seted in config parameter
		if (!(stackLinks.length || currentRequestCount) ||
			!(config.quantity < 1 || collection.pages.length < config.quantity)) {
			saveDataToFile(config.filename, JSON.stringify(collection));
			var finish = new Date();
			console.log('Finish:  ', finish);
			console.log('Total:   ', finish.getTime() - begin.getTime(), 'ms');
			process.exit(0);
		}
		for(i; i<5 && stackLinks.length; i++){
			curr = stackLinks.pop();
			getPage(curr);
		}
		setTimeout(function() {
			spider.emit('ready');
			// console.log('Next iteration');
		}, config.delay); 
	};

//stat main en
//config once
stackLinks.push('/operativ/oper_new.html');//stat main ua
stackLinks.push('/operativ/oper_new_e.html');//stat main en
stackLinks.push('/operativ/oper_new_rus.html');//stat main ru
stackLinks.push('/Noviny/new_u.html');//feed lsat ua
stackLinks.push('/Noviny/new_e.html');//feed lsat en
stackLinks.push('/Noviny/new_r.html');//feed lsat ru

//iterate
stackLinks.push('/Noviny/new2013/new2013_u/new_u04.html');//feed by date ua
stackLinks.push('/Noviny/new2013/new2013_e/new_e04.html');//feed by date en
stackLinks.push('/Noviny/new2013/new2013_r/new_r04.html');//feed by date ru
collection.knownLinks.push.apply(collection.knownLinks, stackLinks);
spider.on('ready', traversePages);
spider.on('pageLoad', parsePage);
config = {
	HOST: 'www.ukrstat.gov.ua',
	quantity: 0,
	delay: 150,
	filename: '/dump.json'
};

//start
console.log('\n\n\n', new Date().toGMTString(), begin);
console.log('Begin:   ', begin);
spider.emit('ready');