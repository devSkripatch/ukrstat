// Статистична інформація -> Житловий фонд -> Житловий фонд України (1990-2011рр.)
/*global $, Highcharts*/

/*
//	select data from "Всього" column (4 at the end of selector)
//	select data from "Весь житловий фонд, загальної площі" column (2 at the end of selector)
//	now with bugs some in parsing
*/
/*var total = document.querySelectorAll('frame')[2].contentDocument.querySelectorAll('#table3 td:nth-child(4)');
var arr_ret = [];
Array.forEach(total, function (el) {
    var text = el.querySelector('font').innerHTML;
    text = text.replace('&nbsp;','');
    text = text.replace(/<sup.+/gim,'');
    text = text.replace(/[^х\d,]/gim,'');
    text = text.replace(/х/gim,'0');
    text = text.replace(/,/gim,'.');
    arr_ret.push(text);
});
alert(arr_ret);*/

var ch_series1 = {
	'small' : [{
		name: 'Кількість організації,<br />які виконують наукові<br />дослідження й розробки',
		data: [1344,1350,1406]/*[null,1344,1350,1406,1463,1453,1435,1450,1518,1506,1490,1479,1477,1487,1505,1510,1452,1404,1378,1340,1303,1255]*/
	}],
	'big' : [{
		name: 'Кількість організації,<br />які виконують наукові<br />дослідження й розробки',
		data: [null,1344,1350,1406,1463,1453,1435,1450,1518,1506,1490,1479,1477,1487,1505,1510,1452,1404,1378,1340,1303,1255]
	}]
},
ch_series2 = {
	'small' : [{
		name: 'Чисельність науковців, осіб',
		data: [313079,295010,248455]/*[null,313079,295010,248455,222127,207436,179799,160103,142532,134413,126045,120773,113341,107447,104841,106603,105512,100245,96820,94138,92403,89534,84969]*/
	}],
	'big' : [{
		name: 'Чисельність науковців, осіб',
		data: [null,313079,295010,248455,222127,207436,179799,160103,142532,134413,126045,120773,113341,107447,104841,106603,105512,100245,96820,94138,92403,89534,84969]
	}]
},
ch_series3 = {
	'small' : [{
		name: 'Чисельність докторів наук<br />в економіці України, осіб',
		data: [8133,8797,9224]/*[null,8133,8797,9224,9441,9759,9974,10322,10446,10233,10339,10603,11008,11259,11573,12014,12488,12845,13423,13866,14418,14895]*/
	}],
	'big' : [{
		name: 'Чисельність докторів наук<br />в економіці України, осіб',
		data: [null,8133,8797,9224,9441,9759,9974,10322,10446,10233,10339,10603,11008,11259,11573,12014,12488,12845,13423,13866,14418,14895]
	}]
},
ch_series4 = {
	'small' : [{
		name: 'Чисельність кандидатів наук<br />в економіці України, осіб',
		data: [57610,58132,59332]/*[null,null,null,null,null,null,57610,58132,59332,59703,59547,58741,60647,62673,64372,6,68291,71893,74191,77763,81169,84000,84979]*/
	}],
	'big' : [{
		name: 'Чисельність кандидатів наук<br />в економіці України, осіб',
		data: [null,null,null,null,null,null,57610,58132,59332,59703,59547,58741,60647,62673,64372,65839,68291,71893,74191,77763,81169,84000,84979]
	}]
};

var common_options = {
	chart: {
		renderTo: 'graph1',
		type: 'line',
		zoomType: 'x'
	},
	title: {
		text: null
	},
	subtitle: {
		text: null
	},
	tooltip: {
		enabled: false
	},
	plotOptions: {
		line: {
			dataLabels: {
				enabled: false
			},
			enableMouseTracking: true
		},
		series: {
			dataLabels: {
				enabled: true,
				style: {
					fontSize: '14px'
				}
			}
		}
	},
	legend: {
		title: {
			text: null
		}
    },
	xAxis: {
		title: {
			text: null
		},
		tickInterval: 1,
		gridLineWidth: 1,
		tickmarkPlacement: 'on',
		categories: ['1990', '1991', '1992']
	},
	yAxis: {
		title: {
			text: null
		},
		minorTickInterval: null,
		gridLineWidth: 0
	},
	series: ch_series1.small
};

$(document).ready(function() {
	var chart1, chart2, chart3, chart4;

	chart1 = new Highcharts.Chart(common_options);

	common_options.chart.renderTo = 'graph2';
	common_options.series = ch_series2.small;
	chart2 = new Highcharts.Chart(common_options);

	common_options.chart.renderTo = 'graph3';
	common_options.series = ch_series3.small;
	chart3 = new Highcharts.Chart(common_options);

	common_options.chart.renderTo = 'graph4';
	common_options.series = ch_series4.small;
	chart4 = new Highcharts.Chart(common_options);
});

// change type of chart
function changeType(chart, series, newType) {
	var i = 0, series_arr = [],	iter_ser,

	ser_length = series.length;

	newType = newType.toLowerCase();

	for ( ; i < ser_length; i+=1 ) {
		iter_ser = series[i];
		try {
			chart.addSeries({
				type: newType,
				stack: iter_ser.stack,
				yaxis: iter_ser.yaxis,
				name: iter_ser.name,
				color: iter_ser.color,
				data: iter_ser.options.data
			}, false);

			series_arr.push(iter_ser);
		} catch (e) {
			alert(newType & ': ' & e);
		}
	}

	$.each(series_arr, function (i, el) {
		el.remove();
	});
}