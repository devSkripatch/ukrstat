// Статистична інформація -> Житловий фонд -> Житловий фонд України (1990-2011рр.)
/*global $, Highcharts*/

/*
//	select data from "Всього" column (4 at the end of selector)
//	select data from "Весь житловий фонд, загальної площі" column (2 at the end of selector)
//	now with bugs some in parsing
*/
/*var total = document.querySelectorAll('frame')[2].contentDocument.querySelectorAll('#table3 td:nth-child(4)');
var arr_ret = [];
Array.forEach(total, function (el) {
    var text = el.querySelector('font').innerHTML;
    text = text.replace('&nbsp;','');
    text = text.replace(/<sup.+/gim,'');
    text = text.replace(/[^х\d,]/gim,'');
    text = text.replace(/х/gim,'0');
    text = text.replace(/,/gim,'.');
    arr_ret.push(text);
});
alert(arr_ret);*/

var chart1,
	ch_series = [{
		name: 'Весь житловий фонд, загальної площі, млн.м<sup>2</sup>',
		data: [922.1,932.7,944.7,960.6,962.9,978.3,995.2,1002.6,1008.4,null,1015.0,1026.1,1031.7,1035.7,1040.0,1046.4,1049.2,1059.90,1066.6,1072.2,1079.5,1086.0]
	}];

$(document).ready(function() {
	chart1 = new Highcharts.Chart({
		chart: {
			renderTo: 'container',
			type: 'line', // line, spline, area, areaspline, column, bar, pie, scatter
            zoomType: 'x'
		},
		title: {
			text: 'Житловий фонд України'
		},
		subtitle: {
			text: 'Весь житловий фонд, загальної площі, млн.м<sup>2</sup><br />select to zoom'
		},
		tooltip: {
			useHTML: true,
			headerFormat: '<strong>За <span>{point.key}</span> рiк<br /></strong>',
			pointFormat: '<strong style="color: {series.color};">{series.name}: </strong><strong>{point.y}</strong><span></span>',
			style: {
				color: '#333333',
				fontSize: '14px',
				padding: '5px'
			}
		},
		plotOptions: {
			line: {
				dataLabels: {
					enabled: false
				},
				enableMouseTracking: true
			},
			series: {
				dataLabels: {
					enabled: true,
					style: {
						fontWeight: 'bold'
					}
				}
			}
		},
		xAxis: {
			title: {
				text: 'За рiк'
			},
			tickInterval: 1,
			gridLineWidth: 1,
			tickmarkPlacement: 'on',
			categories: ['1990', '1991', '1992', '1993', '1994', '1995', '1996', '1997', '1998', '1999', '2000', '2001', '2002', '2003', '2004', '2005', '2006', '2007', '2008', '2009', '2010', '2011']
		},
		yAxis: {
			title: {
				text: 'Площа млн.м<sup>2</sup>'
			},
			minorTickInterval: null /*'auto'*/,
			tickInterval: 1000,
			gridLineWidth: 0
		},
		series: ch_series
	});
});

// change type of chart
function changeType(chart, series, newType) {
	var i = 0, series_arr = [],	iter_ser,

	ser_length = series.length;

	newType = newType.toLowerCase();

	for ( ; i < ser_length; i+=1 ) {
		iter_ser = series[i];
		try {
			chart.addSeries({
				type: newType,
				stack: iter_ser.stack,
				yaxis: iter_ser.yaxis,
				name: iter_ser.name,
				color: iter_ser.color,
				data: iter_ser.options.data
			}, false);

			series_arr.push(iter_ser);
		} catch (e) {
			alert(newType & ': ' & e);
		}
	}

	$.each(series_arr, function (i, el) {
		el.remove();
	});
}