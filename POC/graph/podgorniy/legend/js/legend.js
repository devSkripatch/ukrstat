/*global $*/
$(function () {
	'use strict';

	var legendNode;
	var tailNode;
	var isVisible;
	var isAnimating;

	legendNode = $('.legend');
	tailNode = $('.legend .tail');
	isVisible = false;
	isAnimating = false;

	function toggleLegend () {
		isAnimating = true;
		if (isVisible) {
			isVisible = false;
			legendNode.stop().animate({
				'right' : -400
			}, 'easeOutBack', function () {
				isAnimating = false;
			});
		} else {
			isVisible = true;
			legendNode.stop().animate({
				'right' : 0
			}, 'easeOutBack', function () {
				isAnimating = false;
			});
		}
	}


	function showLegendOnLoad () {
		isAnimating = true;
		legendNode.css('right', -380);
		legendNode.stop().animate({
			'right' : -400
		}, 1000, 'easeOutBounce', function () {
			isAnimating = false;
		});
	}

	legendNode.bind('mouseenter', function () {
		if (isVisible) {
			return;
		}
		isAnimating = true;
		legendNode.stop().animate({
			'right': -390
		}, 500, 'easeInOutCubic', function () {
			isAnimating = false;
		});
	});

	legendNode.bind('mouseleave', function () {
		if (isVisible) {
			return;
		}
		isAnimating = true;
		legendNode.stop().animate({
			'right': -400
		}, 500, 'easeInOutCubic', function () {
			isAnimating = false;
		});
	});

	/*
	 * event handling
	 */
	$(document).bind('click', function () {
		if (isVisible && !isAnimating) {
			toggleLegend();
		}
	});
	legendNode.bind('click', toggleLegend);
	showLegendOnLoad();
});