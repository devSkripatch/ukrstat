var data = require('../models/navigation');

module.exports = function (request, response) {
	'use strict';

	response.render('navigation.jade', data);
};