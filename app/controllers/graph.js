var data = require('../models/graph');

exports.index = function (request, response) {
    response.render('graph.jade', data);
};