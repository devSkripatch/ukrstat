module.exports = function (request, response) {
	'use strict';

	response.statusCode = 404;
	response.render('404');
};