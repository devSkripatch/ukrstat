var data = require('../models/index');
var express = require('express');
var msgbus =require('../msgbus');
var repository = require('viewmodel').read;
var  itemRepo = repository.extend({
        collectionName: 'item'
    });

exports.index = function (request, response) {
     response.render('index.jade', data);
};

exports.set = function (request, response)
{
    msgbus.emitCommand({ id: 'msg1', command: 'createItem', payload: { text: 'xsd33x' }});
    response.render('index.jade', data);
}

exports.get = function(request, response)
{
       itemRepo.find(function(err, items) {
       // response.json(items);
    });

    request.session.visitCount = request.session.visitCount ? request.session.visitCount + 1 : 1;
    response.send('You have visited this page ' + request.session.visitCount + ' times');
}


