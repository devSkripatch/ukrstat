// the hub encapsulates functionality to send or receive messages from redis.

var redis = require('redis')
    , colors = require('./colors')
    , cmdRead = redis.createClient()
    , cmdWrite = redis.createClient()
    , evtRead = redis.createClient()
    , evtWrite = redis.createClient()
    , evtSubscriptions = []
    , cmdSubscriptions = [];

cmdRead.subscribe("commands");
evtRead.subscribe("events");

module.exports = {

    emitCommand: function(command) {
            console.log(colors.blue('\nhub -- publishing command ' + command.command + ' to redis:'));
            console.log(command);
            cmdWrite.publish('commands', JSON.stringify(command));
    },

    onCommand: function(callback) {
        cmdSubscriptions.push(callback);
        console.log(colors.blue('hub -- command subscribers: ' + cmdSubscriptions.length));
    },

    emitEvent: function(event) {
        console.log(colors.blue('\ emitEventnhub -- publishing event ' + event.event + ' to redis:'));
        console.log(event);
        evtWrite.publish('events', JSON.stringify(event));
    },

    onEvent: function(callback) {
        evtSubscriptions.push(callback);
        console.log(colors.blue('hub -- event subscribers: ' + evtSubscriptions.length));
    }

};

// listen to events from redis and call each callback from subscribers
evtRead.on('message', function(channel, message) {
    console.log(colors.green('evtRead.on'));
    var event = JSON.parse(message);

    if (channel === 'events') {

        console.log(colors.green('\ evtRead nhub -- received event ' + event.event + ' from redis:'));
        console.log(event);
        console.log(evtSubscriptions);

        evtSubscriptions.forEach(function(subscriber){
            subscriber(event);
        });
    }
});

// listen to commands from redis and call each callback from subscribers
cmdRead.on('message', function(channel, message) {

    var command = JSON.parse(message);

    if (channel === 'commands') {

        console.log(colors.green('\nhub -- received command ' + command.command + ' from redis:'));
        console.log(command);

        cmdSubscriptions.forEach(function(subscriber){
            subscriber(command);
        });

    }
});

