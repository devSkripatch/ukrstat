
    // server.js is the starting point of the domain process:
//
// `node server.js`
    var colors = require('../colors')
    var msgbus = require('../msgbus');
    var domain = require('cqrs-domain').domain,
        contextEventDenormalizer = require('cqrs-eventdenormalizer').contextEventDenormalizer;


    var options = {
        commandHandlersPath: __dirname + '/commandHandlers',
        aggregatesPath: __dirname + '/aggregates',
        sagaHandlersPath: __dirname + '/sagaHandlers',
        sagasPath: __dirname + '/sagas',
        eventStore: {
            type: 'mongoDb', //'mongoDb',
            dbName: 'ukrstat'
        },
        commandQueue: {
            type: 'mongoDb', //'mongoDb',
            dbName: 'ukrstat'
        },
        repository: {
            type: 'mongoDb', //'mongoDb',
            dbName: 'ukrstat'
        }
    };

    domain.initialize(options, function(err) {
        if(err) {
            console.log(err);
        }

        // on receiving a message (__=command__) from msgbus pass it to
        // the domain calling the handle function
        msgbus.onCommand(function(cmd) {
            console.log(colors.blue('\ndomain -- received command ' + cmd.command + ' from redis:'));
            console.log(cmd);

            console.log(colors.cyan('\n-> handle command ' + cmd.command));

            domain.handle(cmd);
        });

        msgbus.onEvent(function(data) {
            console.log(colors.cyan('eventDenormalizer -- denormalize event ' + data.event));
            contextEventDenormalizer.denormalize(data);
        });

        // on receiving a message (__=event) from domain pass it to the msgbus
        domain.on('event', function(evt) {
            console.log('domai11111n: ' + evt.event);
            msgbus.emitEvent(evt);
        });

        console.log('Starting domain service'.cyan);
    });

