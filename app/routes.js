/**
 * Handlers routing
 */
var controllers = require('./controllers');

module.exports = function (app) {
	'use strict';

	// Example render of index page
	app.all('/index/index', controllers.index.index);
	app.all('/navigation', controllers.navigation); // test navigation
	app.all('/*', controllers.graph.index); // old index
	// All unserverd requests will fall through down to this handler
	app.all('*', controllers['404']);
};