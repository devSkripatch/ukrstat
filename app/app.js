var express = require("express");
var socket = require('socket.io'),
    contextEventDenormalizer = require('cqrs-eventdenormalizer').contextEventDenormalizer,
    repository = require('viewmodel').read;



var app = express(),
     http = require('http'),
     server = http.createServer(app);




var PORT = 8090;
/**
 * Application settings
 */

var options = {
    denormalizersPath: __dirname + '/eventDenormalizers',
    repository: {
        type: 'mongoDb', //'mongoDb',
        dbName: 'ukrstat'
    },
    eventQueue: {
        type: 'mongoDb', //'mongoDb',
        dbName: 'ukrstat'
    }
};


// Temporary view engine is jade, unless we
// change our mind
app.set('view engine', 'jade');

// Moddleware
app.use(express['static'](__dirname + '/static'));
app.use(express.cookieParser());
app.use(express.session({ secret: "ukrstat"}));
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.set('view options', { layout: false });


repository.init(options.repository, function (err) {

    console.log('2. -> eventdenormalizer'.cyan);
    contextEventDenormalizer.initialize(options, function (err) {
        if (err) {
            console.log(err);
        }
        console.log ('Start domain ');
        var domain =  require('./domain/server');


        //Apply routes
        var routes = require('./routes');
        routes(app);

        // Start app
        app.listen(PORT);
        console.log('ukrstat started on ' + PORT + ' port');

    });
});






