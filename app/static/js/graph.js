define(['./mediator'], function(mediator, graphs_data) {
	console.log("graph loaded");
	var container = $('.content');
	var graph = window.graphs_data;

	return function() {
		mediator.subscribe('click', function(data) {
			console.log(data);

			///здесь мы получаем данные с сервера перед отображением графика
			///отправляем запрос с именем графика DATA , получаем в ответ
			/// graph
			/// Синтаксис:
			/// 1. Для графика Имя строки таблицы и цифры  - recieved_graph_data = [{name: String, data: Array of Numbers }]
			///	2. Ед. измерения  - y_coordinates_quantity_unit = "String";
			/// 3. Описание оси координат Y - y_coordinates_description_text = "String";
			/// 4. Описание оси координат Y - x_coordinates_description = [Array of String values];
			/// 5. Количество данных и описаний-Х должно быть равно - recieved_graph_data.data.length === x_coordinates_description.length


//////////////////////////
			/////////////////////////
			////////////////////////
			Highcharts.theme = {
				colors: ["#DDDF0D", "#55BF3B", "#DF5353", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
					"#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
				chart: {
					backgroundColor: {
						linearGradient: {
							x1: 0,
							y1: 0,
							x2: 1,
							y2: 1
						},
						stops: [
							[0, 'rgb(48, 48, 96)'],
							[1, 'rgb(0, 0, 0)']
						]
					},
					borderColor: '#000000',
					borderWidth: 2,
					className: 'dark-container',
					plotBackgroundColor: 'rgba(255, 255, 255, .1)',
					plotBorderColor: '#CCCCCC',
					plotBorderWidth: 1
				},

				title: {
					style: {
						color: '#C0C0C0',
						font: 'bold 16px "Trebuchet MS", Verdana, sans-serif'
					}
				},
				subtitle: {
					style: {
						color: '#666666',
						font: 'bold 12px "Trebuchet MS", Verdana, sans-serif'
					}
				},
				xAxis: {
					gridLineColor: '#333333',
					gridLineWidth: 1,
					labels: {
						style: {
							color: '#A0A0A0'
						}
					},
					lineColor: '#A0A0A0',
					tickColor: '#A0A0A0',
					title: {
						style: {
							color: '#CCC',
							fontWeight: 'bold',
							fontSize: '12px',
							fontFamily: 'Trebuchet MS, Verdana, sans-serif'

						}
					}
				},
				yAxis: {
					gridLineColor: '#333333',
					labels: {
						style: {
							color: '#A0A0A0'
						}
					},
					lineColor: '#A0A0A0',
					minorTickInterval: null,
					tickColor: '#A0A0A0',
					tickWidth: 1,
					title: {
						style: {
							color: '#CCC',
							fontWeight: 'bold',
							fontSize: '12px',
							fontFamily: 'Trebuchet MS, Verdana, sans-serif'
						}
					}
				},
				tooltip: {
					backgroundColor: 'rgba(0, 0, 0, 0.75)',
					style: {
						color: '#F0F0F0'
					}
				},
				toolbar: {
					itemStyle: {
						color: 'silver'
					}
				},
				plotOptions: {
					line: {
						dataLabels: {
							color: '#CCC'
						},
						marker: {
							lineColor: '#333'
						}
					},
					spline: {
						marker: {
							lineColor: '#333'
						}
					},
					scatter: {
						marker: {
							lineColor: '#333'
						}
					},
					candlestick: {
						lineColor: 'white'
					}
				},
				legend: {
					itemStyle: {
						font: '9pt Trebuchet MS, Verdana, sans-serif',
						color: '#A0A0A0'
					},
					itemHoverStyle: {
						color: '#FFF'
					},
					itemHiddenStyle: {
						color: '#444'
					}
				},
				credits: {
					style: {
						color: '#666'
					}
				},
				labels: {
					style: {
						color: '#CCC'
					}
				},

				navigation: {
					buttonOptions: {
						symbolStroke: '#DDDDDD',
						hoverSymbolStroke: '#FFFFFF',
						theme: {
							fill: {
								linearGradient: {
									x1: 0,
									y1: 0,
									x2: 0,
									y2: 1
								},
								stops: [
									[0.4, '#606060'],
									[0.6, '#333333']
								]
							},
							stroke: '#000000'
						}
					}
				},

				// scroll charts
				rangeSelector: {
					buttonTheme: {
						fill: {
							linearGradient: {
								x1: 0,
								y1: 0,
								x2: 0,
								y2: 1
							},
							stops: [
								[0.4, '#888'],
								[0.6, '#555']
							]
						},
						stroke: '#000000',
						style: {
							color: '#CCC',
							fontWeight: 'bold'
						},
						states: {
							hover: {
								fill: {
									linearGradient: {
										x1: 0,
										y1: 0,
										x2: 0,
										y2: 1
									},
									stops: [
										[0.4, '#BBB'],
										[0.6, '#888']
									]
								},
								stroke: '#000000',
								style: {
									color: 'white'
								}
							},
							select: {
								fill: {
									linearGradient: {
										x1: 0,
										y1: 0,
										x2: 0,
										y2: 1
									},
									stops: [
										[0.1, '#000'],
										[0.3, '#333']
									]
								},
								stroke: '#000000',
								style: {
									color: 'yellow'
								}
							}
						}
					},
					inputStyle: {
						backgroundColor: '#333',
						color: 'silver'
					},
					labelStyle: {
						color: 'silver'
					}
				},

				navigator: {
					handles: {
						backgroundColor: '#666',
						borderColor: '#AAA'
					},
					outlineColor: '#CCC',
					maskFill: 'rgba(16, 16, 16, 0.5)',
					series: {
						color: '#7798BF',
						lineColor: '#A6C7ED'
					}
				},

				scrollbar: {
					barBackgroundColor: {
						linearGradient: {
							x1: 0,
							y1: 0,
							x2: 0,
							y2: 1
						},
						stops: [
							[0.4, '#888'],
							[0.6, '#555']
						]
					},
					barBorderColor: '#CCC',
					buttonArrowColor: '#CCC',
					buttonBackgroundColor: {
						linearGradient: {
							x1: 0,
							y1: 0,
							x2: 0,
							y2: 1
						},
						stops: [
							[0.4, '#888'],
							[0.6, '#555']
						]
					},
					buttonBorderColor: '#CCC',
					rifleColor: '#FFF',
					trackBackgroundColor: {
						linearGradient: {
							x1: 0,
							y1: 0,
							x2: 0,
							y2: 1
						},
						stops: [
							[0, '#000'],
							[1, '#333']
						]
					},
					trackBorderColor: '#666'
				},

				// special colors for some of the
				legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
				legendBackgroundColorSolid: 'rgb(35, 35, 70)',
				dataLabelsColor: '#444',
				textColor: '#C0C0C0',
				maskColor: 'rgba(255,255,255,0.3)'
			};

			// Apply the theme
			var highchartsOptions = Highcharts.setOptions(Highcharts.theme);




			////////////////////////
			///////////////////
			//////////////////////
			var chart;
			chart = new Highcharts.Chart({
				chart: {
					renderTo: 'container',
					type: 'column',
					zoomType: 'x',
					marginRight: 50,
					marginBottom: 25

				},
				title: {
					text: data,
					x: -20 //center
				},
				subtitle: {
					text: 'График построен на основе данных http://ukrstat.gov.ua/',
					x: -20
				},
				xAxis: {
					categories: graph.x_coordinates_description
				},
				yAxis: {
					title: {
						text: graph.y_coordinates_description_text + " " + graph.y_coordinates_quantity_unit
					},
					plotLines: [{
						value: 0,
						width: 1,
						color: '#808080'
					}]
				},
				tooltip: {
					useHTML: true,
					headerFormat: '<strong>За <span>{point.key}</span> рiк<br /></strong>',
					pointFormat: '<strong style="color: {series.color};">{series.name}: </strong><strong>{point.y}</strong><span></span>',

					formatter: function() {
						return '<b>' + this.series.name + '</b><br/>' + this.x + ': ' + this.y + " " + graph.y_coordinates_quantity_unit;
					}
				},
				legend: {
					backgroundColor: '#FCFFC5',
					shadow: true,
					layout: 'vertical',
					align: 'right',
					verticalAlign: 'top',
					x: 120,
					y: 45,
					borderWidth: 1,
					floating: true,
					//enabled: false,
					style: {
						'z-index': '1000'
					},
					itemHiddenStyle: {
						color: '#E00000'
					}
				},
				plotOptions: {
					line: {
						dataLabels: {
							enabled: true,
							style: {
								fontWeight: 'bold',
								color: '#ffffff'
							}
						}
					},
					column: {
						dataLabels: {
							enabled: true,
							style: {
								fontWeight: 'bold'
							}
						}
					}
				},
				series: graph.graph_numbers_data
			});


			//////////////////////
			////////////////////
			/////////////////////
			$('.legend ul').text('');
			for (var i = 0; i < graph.graph_numbers_data.length; i++) {
				$('.legend ul').append('<li data-series-id="' + i + '"><a>' + graph.graph_numbers_data[i].name + '</a></li>');
				//console.log(graph.graph_numbers_data[i].name);
			}

			var tranform_attr = $('.highcharts-legend').attr('transform');
			var X_pos_from_transform_attr = /\d+/.exec(tranform_attr);
			var hiding_timer;
			// console.log(a, b);

			var null_position = X_pos_from_transform_attr[0];
			console.log(null_position);

			$('.highcharts-legend').hide();
			/*$('#toggle-legend').text('Скрыть легенду');*/
			$('.buttons').show();
			$('.legend').show();
			/*	function hide_toggle() {
				$('.new-legend').stop();
				$('.new-legend').animate({
					height: 'toggle'
				}, 1000);
				if ($('#toggle-legend').text() === 'Показать легенду') {
					$('#toggle-legend').text('Скрыть легенду');
				} else {
					$('#toggle-legend').text('Показать легенду');
				}
				hiding_timer = false;
			}*/

			//$('#hide-legend-button').on('click', hide_legend);

			/*$('#toggle-legend').on('click', hide_toggle);*/


			/*	setTimeout(hide_toggle, 1000);*/

			var multi_graph_flag;
			var this_chart = $('#container').highcharts();
			var new_legend_item = $('.legend li');
			var active_graphs = [];

			function hide_all_series(e) {
				var series_length = this_chart.series;
				for (var i = 0; i < active_graphs.length; i++) {
					series_length[active_graphs[i]].hide();
					$('.legend li[data-series-id="'+active_graphs[i]+'"]').find('a').css('color', '#ffffff');
				}
			}

			function show_graph() {
				multi_graph_flag = $('#switch-multi-graph').prop('checked');
				console.log(multi_graph_flag);
				var item_id = $(this).attr('data-series-id');
				var series = this_chart.series[item_id];
				if(!multi_graph_flag) {
					hide_all_series();
				}
				active_graphs.push(item_id);
				console.log(active_graphs);
					series.show();
					$(this).find('a').css('color', 'black');
					//$button.html('Hide series');

			}



			$('#hide-all-series').on('click', hide_all_series);
			new_legend_item.bind('click', show_graph);


			/// Множественный или единичный выбор графиков


			// change type of chart
			// Set type
			$.each(['line', 'column', 'spline', 'area', 'areaspline'], function(i, type) {
				$('#' + type).click(function() {
					for (var i = 0; i < this_chart.series.length; i++) {
						this_chart.series[i].update({
							type: type
						});
					}

					console.log('type - ' + type);
				});
			});



			$(function() {
				'use strict';

				var legendNode;
				var tailNode;
				var isVisible;
				var isAnimating;

				legendNode = $('.legend');
				tailNode = $('.legend .tail');
				isVisible = false;
				isAnimating = false;

				tailNode.css({
					'background': 'url(./img/tail-open.png)'
				});

				function toggleLegend() {
					isAnimating = true;
					if (isVisible) {
						isVisible = false;
						legendNode.stop().animate({
							'right': -400
						}, 5000, 'easeOutBack', function() {
							isAnimating = false;
						});
						tailNode.css({
							'background': 'url(./img/tail-open.png)'
						});
					} else {
						isVisible = true;
						legendNode.stop().animate({
							'right': 0
						}, 'easeOutBack', function() {
							isAnimating = false;
						});
						tailNode.css({
							'background': 'url(./img/tail-close.png)'
						});
					}
				}


				/*legendNode.bind('mouseenter', function() {
					if (isVisible) {
						return;
					}
					isAnimating = true;
					legendNode.stop().animate({
						'right': 0
					}, 500, 'easeInOutCubic', function() {
						isAnimating = false;
					});
				});*/

				/*legendNode.bind('mouseleave', function() {
					if (isVisible) {
						return;
					}
					isAnimating = true;
					legendNode.stop().animate({
						'right': -390
					}, 500, 'easeInOutCubic', function() {
						isAnimating = false;
					});
				});*/

				/*
				 * event handling
				 */
				/*$(document).bind('click', function() {
					if (isVisible && !isAnimating) {
						toggleLegend();
					}
				});*/
				function getCookie(c_name) {
					var i, x, y, ARRcookies = document.cookie.split(";");
					for (i = 0; i < ARRcookies.length; i++) {
						x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
						y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
						x = x.replace(/^\s+|\s+$/g, "");
						if (x == c_name) {
							return unescape(y);
						}
					}
				}

				function setCookie(c_name, value, exdays) {
					var exdate = new Date();
					exdate.setDate(exdate.getDate() + exdays);
					var c_value = escape(value) + ((exdays === null) ? "" : "; expires=" + exdate.toUTCString());
					document.cookie = c_name + "=" + c_value;
				}

				function checkCookie() {
					var username = getCookie("username");
					if (username !== null && username !== "") {
						alert("Welcome again " + username);
					} else {
						username = prompt("Please enter your name:", "");
						if (username !== null && username !== "") {
							setCookie("username", username, 365);
						}
					}
				}

				function showLegendOnLoad() {
					isAnimating = true;
					legendNode.css('right', -380);
					legendNode.stop().animate({
						'right': -400
					}, 1000, 'easeOutBounce', function() {
						isAnimating = false;
					});
					setCookie('show_onload', 'showed', 10);
				}
				tailNode.bind('click', toggleLegend);
				if (!getCookie('show_onload')) {
					showLegendOnLoad();
				}
			});
		});
	};
});