module.exports = {
	name : "Тест навигация",
	pageTitle : "Тестирую добавление навигации",
	testContent : "Some content will be here",
	navigation : {
		description : 'Main wrapper define how will be look our navigation (horizontal or vertical)',

		content : [{
			tag : 'ul',
			classes : 'n-m-level-0 nav nav-pills',
			content : [
				{
					tag : 'li',
					classes : 'active',
					content : [
						{
							tag : 'a',
							attrs : {
								'href' : '#'
							},
							content : 'Главная'
						}
					]
				},
				{
					tag : 'li',
					classes : 'n-m-activator',
					content : [
						{
							tag : 'a',
							attrs : {
								'href' : '#'
							},
							content : 'Графики'
						},
						{
							tag : 'ul',
							classes : 'n-m-level-1 dn',
							attrs : {
								'role' : 'menu'
							},
							content : [
								{
									tag : 'li',
									attrs : {
										'data-submenu-id' : 'submenu-prom'
									},
									content : [
										{
											tag : 'a',
											attrs : {
												'href' : '#'
											},
											content : 'Промисловість'
										},
										{
											tag : 'ul',
											classes : 'n-m-level-2 dn',
											attrs : {
												'id' : 'submenu-prom'
											},
											content : [
												{
													tag : 'li',
													content : [
														{
															tag : 'a',
															attrs : {
																'href' : '#'
															},
															content : 'Індекси промислової продукції в Україні (2007-2013рр.)22.03.2013'
														}
													]
												},
												{
													tag : 'li',
													content : [
														{
															tag : 'a',
															attrs : {
																'href' : '#'
															},
															content : 'Індекси промислової продукції (щомісячна інформація)22.03.2013'
														}
													]
												},
												{
													tag : 'li',
													content : [
														{
															tag : 'a',
															attrs : {
																'href' : '#'
															},
															content : 'Індекси промислової продукції (за періоди з початку року) (щомісячна інформація)22.03.2013'
														}
													]
												},
												{
													tag : 'li',
													content : [
														{
															tag : 'a',
															attrs : {
																'href' : '#'
															},
															content : 'Обсяг реалізованої промислової продукції за видами діяльності (щомісячна інформація)'
														}
													]
												},
												{
													tag : 'li',
													content : [
														{
															tag : 'a',
															attrs : {
																'href' : '#'
															},
															content : 'Індекс обороту (реалізації) продукції добувної та переробної промисловості (щомісячна інформація)'
														}
													]
												},
												{
													tag : 'li',
													content : [
														{
															tag : 'a',
															attrs : {
																'href' : '#'
															},
															content : 'Обсяг нових замовлень на виробництво продукції за окремими видами діяльності переробної промисловості (щомісячна інформація)'
														}
													]
												},
												{
													tag : 'li',
													content : [
														{
															tag : 'a',
															attrs : {
																'href' : '#'
															},
															content : 'Виробництво основних видів промислової продукції (щомісячна інформація)22.03.2013'
														}
													]
												},
												{
													tag : 'li',
													content : [
														{
															tag : 'a',
															attrs : {
																'href' : '#'
															},
															content : 'Обсяги реалізованої промислової продукції (робіт, послуг) за 2001-2011рр.'
														}
													]
												},
												{
													tag : 'li',
													content : [
														{
															tag : 'a',
															attrs : {
																'href' : '#'
															},
															content : 'Виробництво основних видів промислової продукції (1990-2011рр.)'
														}
													]
												},
												{
													tag : 'li',
													content : [
														{
															tag : 'a',
															attrs : {
																'href' : '#'
															},
															content : 'Індекси промислової продукції (2000-2012рр.)'
														}
													]
												},
												{
													tag : 'li',
													content : [
														{
															tag : 'a',
															attrs : {
																'href' : '#'
															},
															content : 'Виробництво основних видів продукції харчової промисловості фізичними особами-підприємцями за 2010 рік'
														}
													]
												},
												{
													tag : 'li',
													content : [
														{
															tag : 'a',
															attrs : {
																'href' : '#'
															},
															content : 'Методологічні пояснення'
														}
													]
												},
												{
													tag : 'li',
													content : [
														{
															tag : 'a',
															attrs : {
																'href' : '#'
															},
															content : 'Виробництво та розподілення електроенергії, тепла, газу, води'
														}
													]
												},
												{
													tag : 'li',
													content : [
														{
															tag : 'a',
															attrs : {
																'href' : '#'
															},
															content : 'Електробаланс (2001-2010рр.)'
														}
													]
												},
												{
													tag : 'li',
													content : [
														{
															tag : 'a',
															attrs : {
																'href' : '#'
															},
															content : 'Окремі техніко-економічні показники роботи газопостачальних підприємств'
														}
													]
												},
												{
													tag : 'li',
													content : [
														{
															tag : 'a',
															attrs : {
																'href' : '#'
															},
															content : 'Окремі техніко-економічні показники роботи опалювальних котелень і теплових мереж'
														}
													]
												},
												{
													tag : 'li',
													content : [
														{
															tag : 'a',
															attrs : {
																'href' : '#'
															},
															content : 'Окремі техніко-економічні показники роботи водопроводів та окремих водопровідних мереж'
														}
													]
												},
												{
													tag : 'li',
													content : [
														{
															tag : 'a',
															attrs : {
																'href' : '#'
															},
															content : 'Забезпеченість населених пунктів водою та газом'
														}
													]
												}
											]
										}
									]
								}
							]
						}
					]
				},
				{
					tag : 'li',
					content : [
						{
							tag : 'a',
							attrs : {
								'href' : '#'
							},
							content : 'О нас'
						}
					]
				},
				{
					tag : 'li',
					content : [
						{
							tag : 'a',
							attrs : {
								'href' : '#'
							},
							content : 'Контакты'
						}
					]
				}
			]
		}]
	}
};