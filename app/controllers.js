/**
 * Loads al avaliable controllers in memory
 */
var fs = require('fs');
var CONTROLLERS_DIR = './controllers';

// Load all controllers in memory
var controllers = {};
fs.readdirSync(CONTROLLERS_DIR).forEach(function (controllerFile) {
	'use strict';
	var controllerName;

	console.log('Loading controller %s', CONTROLLERS_DIR + '/' + controllerFile);
	controllerName = controllerFile.split('.')[0];
	controllers[controllerName] = require(CONTROLLERS_DIR + '/' + controllerFile);
});
module.exports = controllers;